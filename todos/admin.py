from django.contrib import admin
from .models import TodoList, TodoItem

# Register your models here.
admin.site.register(TodoList, list_display=["name", "id"])

admin.site.register(
    TodoItem, list_display=["task", "due_date", "is_compeleted"]
)
