from django.shortcuts import (
    render,
    get_list_or_404,
    redirect,
    get_object_or_404,
)
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoListItemForm


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    form = TodoListForm(request.POST or None)
    if form.is_valid():
        item = form.save(commit=False)
        item.list = todo_list
        item.save()
        return redirect("todo_list_detail", id=id)
    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    form = TodoListForm(request.POST or None)
    if form.is_valid():
        todo_list = form.save()
        return redirect("todo_list_detail", id=todo_list.id)
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, "todos/todo_list_update.html", {"form": form})


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    form = TodoListItemForm(request.POST or None)
    if form.is_valid():
        item = form.save()
        return redirect("todo_list_detail", id=item.list.id)
    context = {"form": form}
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoListItemForm(instance=todo_item)
    return render(request, "todos/todo_item_update.html", {"form": form})
